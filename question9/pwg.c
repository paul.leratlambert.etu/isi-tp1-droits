#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include "pwg.h"
#include "check_pass.c"

static const char *username;
static const char *password;

int getUsername()
{
    __uid_t uid = getuid();
    struct passwd* pw = getpwuid(uid);
    if(pw == NULL){
        perror("getpwuid error: ");
    }
    username = pw->pw_name;
}   

int getLineOfUser()
{
    FILE *f;
    char line[1024];
    char *u;
    int i;

    i = 0;
    f = fopen(PATH, "r");
    while(fscanf(f, "%s", line) != EOF){
        u = strtok(line, ":");
        if (strcmp(u, username) == 0) 
        {
            fclose(f);
            return i;
        }
        printf("5\n");
        i = i + 1;
    }
    fclose(f);
    return -1;
}

int replacePassword(int lineToReplace)
{
    FILE * fPtr;
    FILE * fTemp;
    char line[1024];
    char *currentPassword;
    char *readedPassword;
    int count;

    fPtr  = fopen(PATH, "r");
    fTemp = fopen("tmp", "w"); 
    
    if (fPtr == NULL || fTemp == NULL)
    {
        printf("\nUnable to open file.\n");
        printf("Please check whether file exists and you have read/write privilege.\n");
        exit(EXIT_FAILURE);
    }

    count = 0;
    while(fscanf(fPtr, "%s", line) != EOF)
    {
        if (count == lineToReplace)
        {
            currentPassword = strtok(line, ":");
            currentPassword = strtok(NULL, ":");
            readedPassword = readPassword(username);
            if (strcmp(currentPassword, readedPassword) == 0)
            {
                printf("New  ");
                fputs(username, fTemp);
                fputs(":", fTemp);
                fputs(readPassword(username), fTemp);
                fputs("\n", fTemp);            
            }
        }
        else
        {
            fputs(line, fTemp);
        }
        count++;
    }


    /* Close all files to release resource */
    fclose(fPtr);
    fclose(fTemp);


    /* Delete original source file */
    remove(PATH);

    /* Rename temporary file as original file */
    rename("tmp", PATH);

    return 0;
}

int addPassword()
{
    FILE * fPtr;
    char *readedPassword;

    fPtr  = fopen(PATH, "a");
    
    if (fPtr == NULL )
    {
        printf("\nUnable to open file.\n");
        printf("Please check whether file exists and you have read/write privilege.\n");
        exit(EXIT_FAILURE);
    }
    fputs(username, fPtr);
    fputs(":", fPtr);
    fputs(readPassword(username), fPtr);
    fputs("\n", fPtr);            
    
    fclose(fPtr);

    return 1;
}

int main(int argc, char const *argv[])
{  
    int lineOfUser;

    getUsername();
    lineOfUser = getLineOfUser();
    if (lineOfUser != -1)
    {   
        printf("You are going to change your password.\n");
        printf("Please type your current password.\n");
        replacePassword(lineOfUser);
    }
    else
    {
        if(addPassword())
        {
            printf("Password perfectly set !\n");
        }
    }
    
}
