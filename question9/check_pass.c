#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <crypt.h>
#include "check_pass.h"

char * readPassword(const char *username)
{   
    return crypt(getpass("password :\n"), username);
}

int checkPassWord(const char *username, const char *password)
{
    FILE *f;
    char line[1024];
    char *u;
    char *p;
    int i;

    f = fopen(PATH, "r");
    while(fscanf(f, "%s", line) != EOF){
        u = strtok(line, ":");
        p = strtok(NULL,":");
        if ((strcmp(u, username) == 0) && (strcmp(p, password) == 0)) 
        {
            fclose(f);
            return 1;
        } 
    }
    fclose(f);
    return 0;
}