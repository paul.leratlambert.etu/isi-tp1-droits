#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <stdio.h>
#include <errno.h> 
#include <sys/stat.h>
#include <string.h>
#include "check_pass.c"
#include "rmg.h"

static const char *username;
static const char *groupOfFile;
static const char *password;

int getGroupOfFile(const char *filename)
{
    struct stat info;
    stat(filename, &info);  // Error check omitted
    struct passwd *pw = getpwuid(info.st_uid);
    struct group  *gr = getgrgid(info.st_gid);

    groupOfFile = gr->gr_name;

    return 1; 
}

int checkIfUserBelongsToGroup(const char * groupOfFile)
{
    __uid_t uid = getuid();
    struct passwd* pw = getpwuid(uid);
    if(pw == NULL){
        perror("getpwuid error: ");
    }
    username = pw->pw_name;
    int ngroups = 0;

    getgrouplist(pw->pw_name, pw->pw_gid, NULL, &ngroups);
    __gid_t groups[ngroups];

    getgrouplist(pw->pw_name, pw->pw_gid, groups, &ngroups);

    for (int i = 0; i < ngroups; i++){
        struct group* gr = getgrgid(groups[i]);
        if(gr == NULL){
            perror("getgrgid error: ");
        }
        if(strcmp(groupOfFile, gr->gr_name) == 0)
        {
            return 1;
        }
    }
    return 0;
}   

int main(int argc, char const *argv[])
{   
    const char *filename;
    if (argc < 1)
    {
        printf("Usage : ./rmg FILE");
    }
    else
    {
        filename = argv[1];
        getGroupOfFile(filename);
        if (checkIfUserBelongsToGroup(groupOfFile))
        {   
            password = readPassword();
            if (checkPassWord(username, password) && (access(filename, W_OK) == 0 ))
            {
                remove(filename);
                return 0;
            }
            printf("You don't have right to delete this file.\n");
        }
        else
        {
            printf("Error : User and file does not have the same groups.\n");
        }
    }
    return 0;
}
