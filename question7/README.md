# Consignes avant de lancer les scripts

## Pour le script_user_a

Ce mettre à la racine du dossier question7 et utilisant l'utilisateur lambda_a

```bash
su lambda_a
```

## Pour le script_user_b

Ce mettre à la racine du dossier question7 et utilisant l'utilisateur lambda_b

```bash
su lambda_b
```

## Pour le script_admin2

Ce mettre à la racine du dossier question7 et utilisant l'utilisateur admin2  
Nous avons dû créer admin2 et non admin car celui ci était déjà pris.

```bash
su admin2
```

