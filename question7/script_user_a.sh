#!/bin/bash

rm -d dir_a/nouveau_dossier_a
rm dir_a/nouveau_fichier_a.txt


echo -e "\n\e[36mlire tous les fichiers et sous-répertoires contenus dans dir_a et dir_c\e[39m"

echo -e "\n\e[36mls -la sur Le dossier dir_a : \e[39m"
ls -la dir_a

echo -e "\n\e[36mnls -la sur le dossier dir_b : (Permission denied)\e[39m"
ls -la dir_b

echo -e "\n\e[36mnls -la sur le dossier dir_c :\e[39m"
ls -la dir_c

echo -e "\n--------------------------------------------------------\n"

echo -e "\n\e[36mPeuvent lire, mais ne peuvent pas modifier les fichiers dans dir_c, ni les renommer, ni les effacer,
ni créer des nouveaux fichiers\e[39m"

echo -e "\n\e[36mLire les fichier de dir_c :\e[39m"
cat dir_c/admin2.txt

echo -e "\n \e[36mImpossible de modifier : (Permission denied) \e[39m"
echo "coucou" > dir_c/admin2.txt

echo -e "\n\e[36m Impossible de renommer : (Operation not permitted) \e[39m"
mv dir_c/admin2.txt dir_c/changeDeNom.txt

echo -e "\n\e[36mImpossible de supprimer : (Permission denied)\e[39m "
rm dir_c/admin2.txt

echo -e "\n\e[36mImpossible de creer : (Permission denied)\e[39m "
touch dir_c/creerFichier.txt


echo -e "\n--------------------------------------------------------\n"

echo -e "\n\e[36mPeuvent modifier tous les fichiers contenus dans l’arborescence à partir de dir_a, et peuvent créer
de nouveaux fichiers et répertoires dans dir_a\e[39m"

cd dir_a/

echo -e "\n\e[36m Créer nouveau_dossier_a \e[39m"
mkdir nouveau_dossier_a

echo -e "\n\e[36mCréer nouveau_fichier_a.txt \e[39m"
touch nouveau_fichier_a.txt
touch lambda_a-test.txt



echo -e "\n--------------------------------------------------------\n"

echo -e "\n\e[36mN’ont pas le droit d’effacer, ni de renommer, des fichiers dans dir_a qui ne leur appartiennent pas\e[39m"

echo -e "\n\e[36mSupprimer fichier_admin2.txt\e[39m "
rm fichier_admin2_a.txt

echo -e "\n\e[36mRenommer fichier_admin2.txt \e[39m"
mv fichier_admin2_a.txt renommerFichier.txt

cd ../

echo -e "\n--------------------------------------------------------\n"

echo -e "\n\e[36mNe peuvent pas ni lire, ni modifier, ni effacer les fichiers dans dir_b, et ne peuvent pas créer des
nouveaux fichiers dans dir_b\e[39m"


echo -e "\n\e[36mImpossible de lire les fichier de dir_b : (Permission denied)\e[39m"
cat dir_b/lambda_b-test.txt

echo -e "\n\e[36m Impossible de modifier : (Permission denied)\e[39m "
echo "coucou" > dir_b/lambda_b-test.txt

echo -e "\n\e[36mImpossible de renommer : (Permission denied)\e[39m "
mv dir_b/lambda_b-test.txt dir_b/changeDeNom.txt

echo -e "\n\e[36mImpossible de supprimer : (Permission denied) \e[39m"
rm dir_b/lambda_b-test.txt

echo -e "\n\e[36mImpossible de creer : (Permission denied)\e[39m "
touch dir_b/creerFichier.txt