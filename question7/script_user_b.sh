#!/bin/bash

rm -d dir_b/nouveau_dossier_b
rm dir_b/nouveau_fichier_b.txt


echo -e "\n\e[36mlire tous les fichiers et sous-répertoires contenus dans dir_b et dir_c\e[39m"

echo -e "\n\e[36mls -la sur Le dossier dir_b : \e[39m"
ls -la dir_b

echo -e "\n\e[36mls -la sur le dossier dir_a : (Permission denied)\e[39m"
ls -la dir_a

echo -e "\n\e[36mls -la sur le dossier dir_c :\e[39m"
ls -la dir_c

echo -e "\n--------------------------------------------------------\n"

echo -e "\n\e[36mPeuvent lire, mais ne peuvent pas modifier les fichiers dans dir_c, ni les renommer, ni les effacer,
ni créer des nouveaux fichiers\e[39m"

echo -e "\n\e[36mLire les fichier de dir_c :\e[39m"
cat dir_c/admin2.txt

echo -e "\n\e[36mImpossible de modifier : (Permission denied)\e[39m "
echo "coucou" > dir_c/admin2.txt

echo -e "\n\e[36mImpossible de renommer : (Operation not permitted)\e[39m "
mv dir_c/admin2.txt dir_c/changeDeNom.txt

echo -e "\n\e[36mImpossible de supprimer : (Permission denied)\e[39m "
rm dir_c/admin2.txt

echo -e "\n\e[36mImpossible de creer : (Permission denied)\e[39m "
touch dir_c/creerFichier.txt


echo "\n--------------------------------------------------------\n"

echo -e "\n\e[36mPeuvent modifier tous les fichiers contenus dans l’arborescence à partir de dir_b, et peuvent créer
de nouveaux fichiers et répertoires dans dir_b\e[39m"

cd dir_b/

echo -e "\n\e[36mCréer nouveau_dossier_b\e[39m "
mkdir nouveau_dossier_b

echo -e "\n\e[36mCréer nouveau_fichier_b.txt\e[39m "
touch nouveau_fichier_b.txt
touch lambda_b-test.txt



echo -e "\n--------------------------------------------------------\n"

echo -e "\n\e[36mN’ont pas le droit d’effacer, ni de renommer, des fichiers dans dir_b qui ne leur appartiennent pas"

echo -e "\n\e[36mSupprimer fichier_admin2_b.txt \e[39m"
rm fichier_admin2_b.txt

echo -e "\n\e[36mRenommer fichier_admin2_b.txt\e[39m "
mv fichier_admin2_b.txt renommerFichier.txt

cd ../

echo -e "\n--------------------------------------------------------\n"

echo -e "\n\e[36mNe peuvent pas ni lire, ni modifier, ni effacer les fichiers dans dir_a, et ne peuvent pas créer des
nouveaux fichiers dans dir_a\e[39m"


echo -e "\n\e[36mImpossible de lire les fichier de dir_a : (Permission denied)\e[39m"
cat dir_a/lambda_a-test.txt

echo -e "\n\e[36mImpossible de modifier : (Permission denied)\e[39m "
echo "coucou" > dir_a/lambda_a-test.txt

echo -e "\n\e[36mImpossible de renommer : (Permission denied)\e[39m "
mv dir_a/lambda_a-test.txt dir_a/changeDeNom.txt

echo -e "\n\e[36mImpossible de supprimer : (Permission denied)\e[39m "
rm dir_a/lambda_a-test.txt

echo -e "\n\e[36mImpossible de creer : (Permission denied) \e[39m"
touch dir_a/creerFichier.txt