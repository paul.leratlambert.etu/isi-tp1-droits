#!/bin/bash

echo -e "\n\e[36mlire tous les fichiers et sous-répertoires contenus dans dir_a, dir_b et dir_c\e[39m"

echo -e "\n\e[36mls -la sur Le dossier dir_a : \e[39m"
ls -la dir_a

echo -e "\n\e[36mls -la sur le dossier dir_b :\e[39m"
ls -la dir_b

echo -e "\n\e[36mls -la sur le dossier dir_c :\e[39m"
ls -la dir_c

echo -e "\n--------------------------------------------------------\n"

echo -e "\n\e[36mPeut lire, modifier, effacer les fichiers dans dir_a, dir_b et dir_c\e[39m"

echo -e "\n\e[36mLire les fichier de dir_a :\e[39m"
cat dir_a/lambda_a-test.txt

echo -e "\n\e[36Modifier les fichier de dir_a : \e[39m "
echo "coucou" > dir_a/lambda_a-test.txt

echo -e "\n\e[36mRenommer les fichier de dir_a : \e[39m "
echo -e "\n\e[36mAVANT DE RENOMMER :\e[39m"
ls -la
mv dir_a/lambda_a-test.txt dir_a/changeDeNom.txt
echo -e "\n\e[36mAPRES AVOIR RENOMME :\e[39m"
ls -la
#On remet le même nom ensuite pour ne pas perturver les autres tests
mv dir_a/changeDeNom.txt dir_a/lambda_a-test.txt

echo -e "\n\e[36mSupprimer dans le dossier dir_a: \e[39m "
rm dir_a/lambda_a-test.txt

echo -e "\n\e[36mLire les fichier de dir_b :\e[39m"
cat dir_b/lambda_b-test.txt

echo -e "\n\e[36Modifier les fichier de dir_b : \e[39m "
echo "coucou" > dir_b/lambda_b-test.txt

echo -e "\n\e[36mRenommer les fichier de dir_b : \e[39m "
echo -e "\n\e[36mAVANT DE RENOMMER :\e[39m"
ls -la dir_b/
mv dir_b/lambda_b-test.txt dir_b/changeDeNom.txt
echo -e "\n\e[36mAPRES AVOIR RENOMME :\e[39m"
ls -la dir_b/

#On remet le même nom ensuite pour ne pas perturver les autres tests
mv dir_b/changeDeNom.txt dir_b/lambda_b-test.txt

echo -e "\n\e[36mSupprimer dans le dossier dir_b: \e[39m "
rm dir_b/lambda_b-test.txt


echo -e "\n\e[36mCreer dans le dossier dir_c : \e[39m"
touch dir_c/creerFichier_admin2_c.txt

