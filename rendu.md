# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Lerat-Lambert, Paul, paul.leratlambert.etu@univ-lille.fr

- Nom, Prénom, email: Snapp, Yohan, yohan.snapp.etu@univ-lille.fr

## Arboresence du TP

Les question on été réalisé sur la VM de Yohan Snapp voici le TREE de sa machine.

```bash
└── ISI
    ├── exo3
    │   ├── Makefile.mk
    │   ├── suid
    │   └── suid.c
    ├── mydir
    │   └── data.txt
    ├── question4
    │   └── suid.py
    ├── question7
    │   ├── dir_a [error opening dir]
    │   ├── dir_b [error opening dir]
    │   ├── dir_c
    │   │   ├── admin2.txt
    │   │   ├── creerFichier.txt
    │   │   ├── creerFichier_admin2_c.txt
    │   │   └── test.txt
    │   ├── script_user_a.sh
    │   ├── script_user_admin2.sh
    │   └── script_user_b.sh
    ├── question8
    │   ├── Makefile
    │   ├── check_pass.c
    │   ├── check_pass.h
    │   ├── rmg
    │   ├── rmg.c
    │   ├── rmg.h
    │   └── scripttest.sh
    ├── question9
    │   ├── Makefile
    │   ├── check_pass.c
    │   ├── check_pass.h
    |   ├── pwg
    │   ├── pwg.c
    │   ├── pwg.h
    │   └── scripttest.sh
```

## Question 1

Oui le processus peut érire. En effet toto appartiens au groupe **ubuntu** or tous les utilisiateur de ce groupe on le droit de lecture et d'ecriture.

## Question 2

Pour toto, il est impossible d'acceder au repertoire En effet on recoit une réponse :  

```bash
sh: 1: cd can't cd to my dir
```

Pour la commande ls -la mydir, on obtient la liste suivante :  
```bash
ls: cannot access 'mydir/.': Permission denied
ls: cannot access 'mydir/..': Permission denied
ls: cannot access 'mydir/data.txt': Permission denied
total 0
d????????? ? ? ? ? ? .
d????????? ? ? ? ? ? ..
-????????? ? ? ? ? ? data.txt
```
L'utilisateur toto peut lister le contenu mais ne peut pas avoir les détails de celui-ci. 

## Question 3

Lorsque toto lance le programme les valeurs des différents ids sont :   

* **EUID :** 1001
* **EGID :** 1001
* **RUID :** 1001
* **RGID :** 1001

Cependant le programme n'arrive pas a lire le contenu du  fichier **mydir/data.xt**.  

Après avoir activer le tag **set-user-id**, les valeurs des différents ids sont :  

* **EUID :** 1000
* **EGID :** 1001
* **RUID :** 1001
* **RGID :** 1001

Et cette fois le programme arrive a lire le contenu du fichier **mydir/data.xt**.  

## Question 4

Lorsque le script python est apellé par toto, les valeurs des différents ids sont : 

* **EUID :** 1001
* **EGID :** 1001

## Question 5

Cette commande permet de modifier les informations personnel des utilisateurs contenu dans le fichier "/etc/passwd".  
Syntaxe:  
chfn \[Options] nom_utilisateur
  
Les options
  
- f => Nom complet de l'utilisateur
- r => N° de bureau
- w => Téléphone de bureau
- h => Téléphone personnel
- o => Autre information  
  
la commande : ls -al /usr/bin/chfn à les droits suivant :

```bash
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
```

- "-" -> c'est un fichier executable
- rws -> le ***user*** à les droits suivant : read, write, execute, le s veut dire que le setuid est appliqué, ce qui engendre que tous les utilisateurs capable d'executer le fichier, l'executerons avec les droits du propriétaire.  
- r-x -> le ***group*** à les droits suivant : read, execute  
- r-x -> le ***other*** à les droits suivant : read, execute  


Avant commande chfn :  
toto:x:1001:1001:toto,,,:/home/toto:/bin/bash  
Après modification :   
toto:x:1001:1001:toto,1,0123456789,0987654321,other:/home/toto:/bin/bash  

## Question 6

Les mots de passe dans les système Unix/Linux sont stockés en étant hashé dans le fichier */etc/shadow*.  
Le système utilise une methode de hash(MD5) pour crypter les mots de passe.

## Question 7
```bash
mkdir question7
sudo groupadd groupe_a
sudo groupadd groupe_b
sudo adduser lambda_a
sudo usermod -g groupe_a lambda_a
sudo useradd lambda_b
sudo usermod -g groupe_b lambda_b
#On crée un admin2 car admin est déjà pris
sudo useradd admin2
#On rajoute le groupe_b et groupe_a à l'admin2
sudo usermod -g groupe_a admin2
sudo usermod -a -G groupe_b admin2

#On crée un groupe_c pour mettre les users lambda_a & lambda_b dans le meme groupe
sudo groupadd groupe_c
sudo usermod -a -G groupe_c lambda_a
sudo usermod -a -G groupe_c lambda_b

#On crée les directories
sudo mkdir dir_a
sudo mkdir dir_b
sudo mkdir dir_c

#Commande pour changer le owner du group
sudo chgrp groupe_a dir_a
sudo chgrp groupe_b dir_b
sudo chgrp groupe_c dir_c

#On donne l'accès au groupe concerné et a l'admin mais pas aux autre
     
sudo chmod -R 770 dir_b
sudo chmod -R 770 dir_c  


sudo usermod -g admin2 admin2
sudo usermod -a -G groupe_b admin2

#On change le owner des directory pour que admin2 puisse renomer ou supprimer les fichiers aussi
sudo chown admin2:groupe_a dir_a
sudo chown admin2:groupe_b dir_b

```
Mettre les scripts bash dans le repertoire *question7*.

## Question 8
Implémentation de RMG

### rmg.h

```c
int getGroupOfFile(const char *filename); /* Get group of fie */
int checkIfUserBelongsToGroup(const char * groupOfFile); /* Check if the user belongs to the good group */
```
### rmg.c

```c
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include <stdio.h>
#include <errno.h> 
#include <sys/stat.h>
#include <string.h>
#include "check_pass.c"
#include "rmg.h"

static const char *username;
static const char *groupOfFile;
static const char *password;

int getGroupOfFile(const char *filename)
{
    struct stat info;
    stat(filename, &info);  // Error check omitted
    struct passwd *pw = getpwuid(info.st_uid);
    struct group  *gr = getgrgid(info.st_gid);

    groupOfFile = gr->gr_name;

    return 1; 
}

int checkIfUserBelongsToGroup(const char * groupOfFile)
{
    __uid_t uid = getuid();
    struct passwd* pw = getpwuid(uid);
    if(pw == NULL){
        perror("getpwuid error: ");
    }
    username = pw->pw_name;
    int ngroups = 0;

    getgrouplist(pw->pw_name, pw->pw_gid, NULL, &ngroups);
    __gid_t groups[ngroups];

    getgrouplist(pw->pw_name, pw->pw_gid, groups, &ngroups);

    for (int i = 0; i < ngroups; i++){
        struct group* gr = getgrgid(groups[i]);
        if(gr == NULL){
            perror("getgrgid error: ");
        }
        if(strcmp(groupOfFile, gr->gr_name) == 0)
        {
            return 1;
        }
    }
    return 0;
}   

int main(int argc, char const *argv[])
{   
    const char *filename;
    if (argc < 1)
    {
        printf("Usage : ./rmg FILE");
    }
    else
    {
        filename = argv[1];
        getGroupOfFile(filename);
        if (checkIfUserBelongsToGroup(groupOfFile))
        {   
            password = readPassword();
            if (checkPassWord(username, password) && (access(filename, W_OK) == 0 ))
            {
                remove(filename);
                return 0;
            }
            printf("You don't have right to delete this file.\n");
        }
        else
        {
            printf("Error : User and file does not have the same groups.\n");
        }
    }
    return 0;
}

```
### check_pass.h

```c
#define PATH "/home/admin2/passwd"

const char * readPassword(); /* Asks for the user's password */
int checkPassWord(const char *username, const char *password); /* Check password */
```
### check_pass.c

```c
#include <unistd.h>
#include "check_pass.h"

const char * readPassword()
{
     return getpass("Enter your password :\n");
}

int checkPassWord(const char *username, const char *password)
{
    FILE *f;
    char line[1024];
    char *u;
    char *p;
    int i;

    f = fopen(PATH, "r");
    while(fscanf(f, "%s", line) != EOF){
        u = strtok(line, ":");
        p = strtok(NULL,":");
        if ((strcmp(u, username) == 0) && (strcmp(p, password) == 0)) 
        {
            return 1;
        } 
    }
    fclose(f);
    return 0;
}
```

Une fois notre programme compiler on se connecte sur root afin d'executer la commande suivante :
```bash
chmod +s /home/ubuntu/ISI/rmg/rmg
```
Grâce à cela l'executable s'execute avec les droits admin2 et peut donc lire le fichier **/home/admin2/passwd**.

## Question 9

Nos script fonctionne en local, cependant, sur la machine virtuelle on a des probleme au niveau de la suppression de notre fichier testfile, nous avons avons le message "You don't have right to delete this file." alors que l'utilisateur a bien les droits de suppression.

Implémentation de PWG.

## pwg.h
```c
#define BUFFER_SIZE 1024
#define PATH "/home/admin2/passwd"
```

## pwg.c
```c
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pwd.h>
#include "pwg.h"
#include "check_pass.c"

static const char *username;
static const char *password;

int getUsername()
{
    __uid_t uid = getuid();
    struct passwd* pw = getpwuid(uid);
    if(pw == NULL){
        perror("getpwuid error: ");
    }
    username = pw->pw_name;
}   

int getLineOfUser()
{
    FILE *f;
    char line[1024];
    char *u;
    int i;

    i = 0;
    f = fopen(PATH, "r");
    while(fscanf(f, "%s", line) != EOF){
        u = strtok(line, ":");
        if (strcmp(u, username) == 0) 
        {
            fclose(f);
            return i;
        }
        printf("5\n");
        i = i + 1;
    }
    fclose(f);
    return -1;
}

int replacePassword(int lineToReplace)
{
    FILE * fPtr;
    FILE * fTemp;
    char line[1024];
    char *currentPassword;
    char *readedPassword;
    int count;

    fPtr  = fopen(PATH, "r");
    fTemp = fopen("tmp", "w"); 
    
    if (fPtr == NULL || fTemp == NULL)
    {
        printf("\nUnable to open file.\n");
        printf("Please check whether file exists and you have read/write privilege.\n");
        exit(EXIT_FAILURE);
    }

    count = 0;
    while(fscanf(fPtr, "%s", line) != EOF)
    {
        if (count == lineToReplace)
        {
            currentPassword = strtok(line, ":");
            currentPassword = strtok(NULL, ":");
            readedPassword = readPassword(username);
            if (strcmp(currentPassword, readedPassword) == 0)
            {
                printf("New  ");
                fputs(username, fTemp);
                fputs(":", fTemp);
                fputs(readPassword(username), fTemp);
                fputs("\n", fTemp);            
            }
        }
        else
        {
            fputs(line, fTemp);
        }
        count++;
    }


    /* Close all files to release resource */
    fclose(fPtr);
    fclose(fTemp);


    /* Delete original source file */
    remove(PATH);

    /* Rename temporary file as original file */
    rename("tmp", PATH);

    return 0;
}

int addPassword()
{
    FILE * fPtr;
    char *readedPassword;

    fPtr  = fopen(PATH, "a");
    
    if (fPtr == NULL )
    {
        printf("\nUnable to open file.\n");
        printf("Please check whether file exists and you have read/write privilege.\n");
        exit(EXIT_FAILURE);
    }
    fputs(username, fPtr);
    fputs(":", fPtr);
    fputs(readPassword(username), fPtr);
    fputs("\n", fPtr);            
    
    fclose(fPtr);

    return 1;
}

int main(int argc, char const *argv[])
{  
    int lineOfUser;

    getUsername();
    lineOfUser = getLineOfUser();
    if (lineOfUser != -1)
    {   
        printf("You are going to change your password.\n");
        printf("Please type your current password.\n");
        replacePassword(lineOfUser);
    }
    else
    {
        if(addPassword())
        {
            printf("Password perfectly set !\n");
        }
    }
    
}

```

## check_pass.h
```c
#define PATH "/home/admin2/passwd"

char * readPassword(const char *usernam); /* Asks for the user's password */
int checkPassWord(const char *username, const char *password); /* Check password */
```

## check_pass.c
```c
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <crypt.h>
#include "check_pass.h"

char * readPassword(const char *username)
{   
    return crypt(getpass("password :\n"), username);
}

int checkPassWord(const char *username, const char *password)
{
    FILE *f;
    char line[1024];
    char *u;
    char *p;
    int i;

    f = fopen(PATH, "r");
    while(fscanf(f, "%s", line) != EOF){
        u = strtok(line, ":");
        p = strtok(NULL,":");
        if ((strcmp(u, username) == 0) && (strcmp(p, password) == 0)) 
        {
            fclose(f);
            return 1;
        } 
    }
    fclose(f);
    return 0;
}
```

Une fois notre programme compiler on se connecte sur root afin d'executer la commande suivante :
```bash
chmod +s /home/ubuntu/ISI/rmg/rmg
```
Grâce à cela l'executable s'execute avec les droits admin2 et peut donc lire le fichier **/home/admin2/passwd**.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








