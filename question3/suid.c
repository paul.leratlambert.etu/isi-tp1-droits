#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int printIDs()
{
    uid_t euid; //geteeuid
    uid_t egid; //getegid
    uid_t ruid; //get uid
    uid_t rgid; //getgid

    euid = geteuid();
    egid = getgid();
    ruid = getuid();
    rgid = getegid();
    printf("euid: %u\n", euid);
    printf("egid: %u\n", egid);
    printf("ruid: %u\n", ruid);
    printf("rgid: %u\n", rgid);

    return 0;
}

int readFile(char* fileName)
{
    FILE* f = NULL;
    int currentChar = 0;
 
    f = fopen(fileName, "r");
 
    if (f != NULL)
    {
       do
        {
            currentChar = fgetc(f); // On lit le caractère
            printf("%c", currentChar); // On l'affiche
        } while (currentChar != EOF); // On continue tant que fgetc n'a pas retourné EOF (fin de f)
 
        fclose(f);
    }
 
    return 0;
}

int main(int argc, char *argv[])
{
    printIDs();
    readFile(argv[1]);
    
    return 0;
}
