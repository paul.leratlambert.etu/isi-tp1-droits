import os

def printIDS():
    """
    Print EUID & EGId
    """
    print("EUID : ", os.geteuid())
    print("EGID : ", os.getegid())

if __name__ == "__main__":
    getID()
